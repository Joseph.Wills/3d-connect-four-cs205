import numpy as np
from Game import Game


def main():
    num_rows = 6
    num_cols_x = 6
    num_cols_y = 6

    game = Game(num_rows, num_cols_x, num_cols_y)
    end_game = False
    winner = 0
    turn = 0

    # game.display_board()

    # Begins the loop for creating each players turn
    while not end_game:

        # Player 1 play choice
        if turn == 0:

            column_x = int(input("Pick your play Player 1 by entering (0-6):"))
            column_y = int(input("Pick your play Player 1 by entering (0-6):"))

            if game.valid_move(column_x, column_y):
                game.board = game.move(column_x, column_y, 1)

        # Player 2 play choice
        else:
            column_x = int(input("Pick your play Player 2 by entering (0-6):"))
            column_y = int(input("Pick your play Player 2 by entering (0-6):"))

            if game.valid_move(column_x, column_y):
                game.board = game.move(column_x, column_y, 2)

        # display game and check for winner
        # game.display_board()
        end_game, winner = game.check_game_over()

        # switch player turns
        turn += 1
        turn = turn % 2

    # end game message
    if winner:
        print("Player", winner, "wins!")
    else:
        print("Game ends in a tie!")


main()
