import pygame
import math
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

class Piece:
    def __init__(self, coordinates, color):
        board_size = 2.25
        column_width = board_size/6.0
        column_center = column_width/2.0
        place_x = -5*column_center + coordinates[0]*2*column_center;
        place_y = -5*column_center + coordinates[1]*2*column_center;
        place_z = -2*column_width + coordinates[2]*column_width;

        if color == 0:
            #bottom
            glBegin(GL_POLYGON)
            glColor3f(0.1, 0.1, 0.1)
            for x in range (0, 628):
                i = x*0.01000507214
                glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glEnd()
            
            #top
            glBegin(GL_POLYGON)
            glColor3f(0.1, 0.1, 0.1)
            for x in range (0, 628):
                i = x*0.01000507214     #2pi/628
                glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glEnd()

            #middle
            glBegin(GL_POLYGON)
            glColor3f(0.1, 0.1, 0.1)

            for x in range (0, 628):
                i = x*0.01000507214
                #side length
                glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                
                #side width
                glVertex3f(place_z,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
                
                #side length, than back to the first point
                glVertex3f(place_z-.05,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
            glEnd()
            
            #bottom outline
            glBegin(GL_LINES)
            glColor3f(0, 0, 0)
            for x in range (0, 314):
                i = x*0.0200101443
                glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                glVertex3f(place_z,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
            glEnd()
            
            #top outline
            glBegin(GL_LINES)
            glColor3f(0, 0, 0)
            for x in range (0, 314):
                i = x*0.0200101443
                glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                glVertex3f(place_z-.05,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
            glEnd()
            
        if color == 1:
            #bottom
            glBegin(GL_POLYGON)
            glColor3f(0.9, 0, 0)
            for x in range (0, 628):
                i = x*0.01000507214
                glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glEnd()

            #top
            glBegin(GL_POLYGON)
            glColor3f(0.9, 0, 0)
            for x in range (0, 628):
                i = x*0.01000507214     #2pi/628
                glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glEnd()

            #middle
            glBegin(GL_POLYGON)
            glColor3f(0.9, 0, 0)

            for x in range (0, 628):
                i = x*0.01000507214
                #side length
                glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)

                #side width
                glVertex3f(place_z,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)

                #side length, than back to the first point
                glVertex3f(place_z-.05,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
            glEnd()

            #bottom outline
            glBegin(GL_LINES)
            glColor3f(0.8, 0, 0)
            for x in range (0, 314):
                i = x*0.0200101443
                glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                glVertex3f(place_z,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
            glEnd()

            #top outline
            glBegin(GL_LINES)
            glColor3f(0.8, 0, 0)
            for x in range (0, 314):
                i = x*0.0200101443
                glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
                glVertex3f(place_z-.05,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
            glEnd()
