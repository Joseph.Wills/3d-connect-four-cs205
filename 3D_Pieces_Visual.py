import pygame
import math
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
    
def full_board(size):
    #to draw lines
    glBegin(GL_LINES)
    #black lines
    glColor3f(0, 0, 0)
            
    #center = (0,0,0)
    #size = 2.25
    
    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0
    
    #Render all y-lines
    for z in range(num_lines):
        for x in range(num_lines):
            glVertex3f(-size/2.0+(x*column_width), size/2.0, size/2.0-(z*column_width))
            glVertex3f(-size/2.0+(x*column_width), -size/2.0, size/2.0-(z*column_width))

    #Render all x-lines
    for z in range(num_lines):
        for y in range(num_lines):
            glVertex3f(-size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))
            glVertex3f(size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))

    #Render all z-lines
    for y in range(num_lines):
        for x in range(num_lines):
            glVertex3f(-size/2.0+(x*column_width), size/2.0-(y*column_width), size/2.0)
            glVertex3f(-size/2.0+(x*column_width), size/2.0-(y*column_width), -size/2.0)
    
    glEnd()

def semi_board(size):
    #to draw lines
    glBegin(GL_LINES)
    #black lines
    glColor3f(0, 0, 0)
            
    #center = (0,0,0)
    #size = 2.25

    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0

    for z in range(num_lines):
        #Top lines
        glVertex3f(-size/2.0, size/2.0, size/2.0-(z*column_width))
        glVertex3f(-size/2.0, -size/2.0, size/2.0-(z*column_width))
        
        glVertex3f(-size/2.0, size/2.0-(z*column_width), size/2.0)
        glVertex3f(-size/2.0, size/2.0-(z*column_width), -size/2.0)
        
        #Bottom lines
        glVertex3f(size/2.0, -size/2.0, size/2.0-(z*column_width))
        glVertex3f(size/2.0, size/2.0, size/2.0-(z*column_width))

        glVertex3f(size/2.0, size/2.0-(z*column_width), size/2.0)
        glVertex3f(size/2.0, size/2.0-(z*column_width), -size/2.0)
        

    #Render column lines
    for z in range(num_lines):
        for y in range(num_lines):
            glVertex3f(-size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))
            glVertex3f(size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))

    glEnd()
    
def piece(x,y,z, color):
    place_x = x;
    place_y = y;
    place_z = z;

    if color == "black":
        #bottom
        glBegin(GL_POLYGON)
        glColor3f(0.1, 0.1, 0.1)
        for x in range (0, 628):
            i = x*0.01000507214
            glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
        glEnd()
        
        #top
        glBegin(GL_POLYGON)
        glColor3f(0.1, 0.1, 0.1)
        for x in range (0, 628):
            i = x*0.01000507214     #2pi/628
            glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
        glEnd()

        #middle
        glBegin(GL_POLYGON)
        glColor3f(0.1, 0.1, 0.1)

        for x in range (0, 628):
            i = x*0.01000507214
            #side length
            glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            
            #side width
            glVertex3f(place_z,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
            
            #side length, than back to the first point
            glVertex3f(place_z-.05,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
        glEnd()
        
        #bottom outline
        glBegin(GL_LINES)
        glColor3f(0, 0, 0)
        for x in range (0, 314):
            i = x*0.0200101443
            glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glVertex3f(place_z,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
        glEnd()
        
        #top outline
        glBegin(GL_LINES)
        glColor3f(0, 0, 0)
        for x in range (0, 314):
            i = x*0.0200101443
            glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glVertex3f(place_z-.05,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
        glEnd()
        
    if color == "red":
        #bottom
        glBegin(GL_POLYGON)
        glColor3f(0.9, 0, 0)
        for x in range (0, 628):
            i = x*0.01000507214
            glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
        glEnd()

        #top
        glBegin(GL_POLYGON)
        glColor3f(0.9, 0, 0)
        for x in range (0, 628):
            i = x*0.01000507214     #2pi/628
            glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
        glEnd()

        #middle
        glBegin(GL_POLYGON)
        glColor3f(0.9, 0, 0)

        for x in range (0, 628):
            i = x*0.01000507214
            #side length
            glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)

            #side width
            glVertex3f(place_z,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)

            #side length, than back to the first point
            glVertex3f(place_z-.05,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
        glEnd()

        #bottom outline
        glBegin(GL_LINES)
        glColor3f(0.8, 0, 0)
        for x in range (0, 314):
            i = x*0.0200101443
            glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glVertex3f(place_z,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
        glEnd()

        #top outline
        glBegin(GL_LINES)
        glColor3f(0.8, 0, 0)
        for x in range (0, 314):
            i = x*0.0200101443
            glVertex3f(place_z-.05,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
            glVertex3f(place_z-.05,0.1*math.sin(i+0.0200101443)+place_x,0.1*math.cos(i+0.0200101443)+place_y)
        glEnd()

def draw_axes():
    width = 1000
    height = 1000
    
    glLineWidth(2.0);
    glBegin(GL_LINES);
    #Red x-axis
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(-width/2.0, 0.0, 0.0);
    glVertex3f(width/2.0, 0.0, 0.0);
    #Green y-axis
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, height/2.0, 0.0);
    glVertex3f(0.0, -height/2.0, 0.0);
    #Blue z-axis
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, width*2);
    glVertex3f(0.0, 0.0, -width*2);
    glEnd();

    
def main():
    pygame.init()
    pygame.display.set_caption('3D Connect Four')
    window_size = (850,800)
    screen = pygame.display.set_mode(window_size, DOUBLEBUF|OPENGL)
    
    #gluPerspective(field of view, aspect ratio w/h, near & far clipping plane)
    gluPerspective(45, (window_size[0]/window_size[1]), 0.1, 50.0)
    
    #starting perspective
    glTranslatef(0.0, 0.0, -5)
    glRotatef(90, 0, 110, -110)
    
    board_size = 2.25
    
    #to undo rotate
    rotated_xy = 0
    rotate_zy = 0
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit(); sys.exit()
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                glRotatef(3, 1, 0, 0)
                rotated_xy = rotated_xy + 3
            elif event.key == pygame.K_RIGHT:
                glRotatef(-3, 1, 0, 0)
                rotated_xy = rotated_xy - 3
            elif event.key == pygame.K_UP:
                glRotatef(-3, 0, 0, 1)
                rotate_zy = rotate_zy - 3
            elif event.key == pygame.K_DOWN:
                glRotatef(3, 0, 0, 1)
                rotate_zy = rotate_zy - 3
            #x key undos rotation
            elif event.key == pygame.K_x:
                glRotatef(-rotated_xy, 1, 0, 0)
                glRotatef(-rotate_zy, 0, 0, 1)
                #reset back
                rotated_xy = 0
                rotate_zy = 0
            #z key changes board
            elif event.key == pygame.K_z:
                pygame.display.flip()
                glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
                full_board(board_size)
                    
        #to constantly rotate
        #glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
        #Background Color
        glClearColor(1, 1, 1, 1)
        
        draw_axes()
        
        column_width = board_size/6.0
        column_center = column_width/2.0
        
        color = "red"
        piece(1*column_center,1*column_center,0,color)
        
        color = "black"
        piece(5*column_center,5*column_center,0,color)
        
        color = "red"
        piece(3*column_center,3*column_center,0,color)
        
        color = "black"
        piece(5*column_center,5*column_center,2*column_width,color)
        
        semi_board(board_size)
        #full_board(board_size)
        
        
        pygame.display.flip()
            
main()
