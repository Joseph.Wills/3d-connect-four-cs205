import pygame
import math
import random
import numpy as np
import time
from pygame.locals import *
from Piece import Piece
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

PAINTED_COLORS = [(.9, 0, 0),(0, 0.9, 0),(0, 0, .9),(.5, 0, .5),(.5, .5, 0),(0, 0.5, 0.5)]
COLORS_BYTES = [b'\xe5\x00\x00', b'\x00\xe5\x00', b'\x00\x00\xe5', b'\x7f\x00\x7f', b'\x7f\x7f\x00', b'\x00\x7f\x7f']

threedb = []
players = {1:'red', 2:'black'}
def make_board():
    for i in range(6):
        board = []
        for k in range(6):
            row = []
            for k in range(6):
                row.append(0)#k)
            board.append(row)
        threedb.append(board)
def print_board(b):
    for i in b:
        for k in i:
            print(k)
        print()
def move(x, z, p):
    if p != 1 and p != 2:
        return False
    if not is_column_full(x, z):
        i = 0
        search = True
        while search:
            if i == 6 or threedb[z][i][x] != 0:
                search = False
            else:
                i += 1
        threedb[z][i - 1][x] = p

def is_column_full(x, z):
    count = 0
    for i in range(6):
        if threedb[z][i][x] != 0:
            count += 1
    if count == 6:
        return True
    return False

def transpose():
    tdb = []
    for i in range(6):
        board = []
        for j in range(6):
            row = []
            for k in range(6):
                row.append(threedb[k][j][i])
            board.append(row)
        tdb.append(board)
    return tdb

def test_game_over():
    #up and down
    buffer = buffer_board(transpose()) + buffer_board(threedb)
    if '1111' in buffer:
        return ('red')
    elif '2222' in buffer:
        return ('black')
    else: return('none')
    print(buffer)


def buffer_board(t):
    buffer = ''
    for board in t:
        for i in range(6):
            for k in range(6):
                buffer += str(board[k][i])
            buffer += ' '
    #left and right
    for board in t:
        for i in range(6):
            for k in range(6):
                buffer += str(board[i][k])
            buffer += ' '
    return buffer

def full_board(size):
    #to draw lines
    glBegin(GL_LINES)
    #black lines
    glColor3f(0, 0, 0)
            
    #center = (0,0,0)
    #size = 2.25
    
    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0
    
    #Render all y-lines
    for z in range(num_lines):
        for x in range(num_lines):
            glVertex3f(-size/2.0+(x*column_width), size/2.0, size/2.0-(z*column_width))
            glVertex3f(-size/2.0+(x*column_width), -size/2.0, size/2.0-(z*column_width))

    #Render all x-lines
    for z in range(num_lines):
        for y in range(num_lines):
            glVertex3f(-size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))
            glVertex3f(size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))

    #Render all z-lines
    for y in range(num_lines):
        for x in range(num_lines):
            glVertex3f(-size/2.0+(x*column_width), size/2.0-(y*column_width), size/2.0)
            glVertex3f(-size/2.0+(x*column_width), size/2.0-(y*column_width), -size/2.0)
    
    glEnd()

def semi_board(size):
    #to draw lines
    glBegin(GL_LINES)
    #black lines
    glColor3f(0, 0, 0)
            
    #center = (0,0,0)
    #size = 2.25

    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0

    for z in range(num_lines):
        #Top lines
        glVertex3f(-size/2.0, size/2.0, size/2.0-(z*column_width))
        glVertex3f(-size/2.0, -size/2.0, size/2.0-(z*column_width))
        
        glVertex3f(-size/2.0, size/2.0-(z*column_width), size/2.0)
        glVertex3f(-size/2.0, size/2.0-(z*column_width), -size/2.0)
        
        #Bottom lines
        glVertex3f(size/2.0, -size/2.0, size/2.0-(z*column_width))
        glVertex3f(size/2.0, size/2.0, size/2.0-(z*column_width))

        glVertex3f(size/2.0, size/2.0-(z*column_width), size/2.0)
        glVertex3f(size/2.0, size/2.0-(z*column_width), -size/2.0)
        

    #Render column lines
    for z in range(num_lines):
        for y in range(num_lines):
            glVertex3f(-size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))
            glVertex3f(size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))

    glEnd()
    
def draw_axes():
    width = 1000
    height = 1000
    
    glLineWidth(2.0);
    glBegin(GL_LINES);
    #Red x-axis
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(-width/2.0, 0.0, 0.0);
    glVertex3f(width/2.0, 0.0, 0.0);
    #Green y-axis
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, height/2.0, 0.0);
    glVertex3f(0.0, -height/2.0, 0.0);
    #Blue z-axis
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, width*2);
    glVertex3f(0.0, 0.0, -width*2);
    glEnd();

def column_indicator(x,y):
    height = 0.20
    place_x = x;
    place_y = y;
    place_z = -2.25/2 - height;

    #top
    glBegin(GL_POLYGON)
    glColor3f(0.9, 0.9, 0.0)
    for x in range (0, 628):
        i = x*0.01000507214     #2pi/628
        glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
        glVertex3f(place_z,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
        #point
        glVertex3f(place_z+height,0+place_x,0+place_y)
    glEnd()
    
    glBegin(GL_LINES);
    glColor3f(0.8, 0.8, 0.0)
    for x in range (0, 628):
        i = x*0.01000507214     #2pi/628
        glVertex3f(place_z,0.1*math.sin(i)+place_x,0.1*math.cos(i)+place_y)
        glVertex3f(place_z,0.1*math.sin(i+0.01000507214)+place_x,0.1*math.cos(i+0.01000507214)+place_y)
    glEnd()
    

   
def main():
    pygame.init()
    pygame.display.set_caption('3D Connect Four')
    window_size = (850,800)
    screen = pygame.display.set_mode(window_size, DOUBLEBUF|OPENGL)
    
    #gluPerspective(field of view, aspect ratio w/h, near & far clipping plane)
    gluPerspective(45, (window_size[0]/window_size[1]), 0.1, 50.0)
    
    #starting perspective
    glTranslatef(0.0, 0.0, -5)
    glRotatef(90, 0, 110, -110)

    full_size = False

    board_size = 2.25
    click_coords = (0, 0)
    turn_taken = False
    #to undo rotate
    rotated_xy = 0
    rotate_zy = 0
    while test_game_over() == 'none':
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit(); sys.exit()
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                glRotatef(3, 1, 0, 0)
                rotated_xy = rotated_xy + 3
            elif event.key == pygame.K_RIGHT:
                glRotatef(-3, 1, 0, 0)
                rotated_xy = rotated_xy - 3
            elif event.key == pygame.K_UP:
                glRotatef(-3, 0, 0, 1)
                rotate_zy = rotate_zy - 3
            elif event.key == pygame.K_DOWN:
                glRotatef(3, 0, 0, 1)
                rotate_zy = rotate_zy - 3
            #x key undos rotation
            elif event.key == pygame.K_x:
                glRotatef(-rotated_xy, 1, 0, 0)
                glRotatef(-rotate_zy, 0, 0, 1)
                #reset back
                rotated_xy = 0
                rotate_zy = 0
            #z key changes board
            elif event.key == pygame.K_z:
                pygame.display.flip()
                glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
                full_board(board_size)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                pass
            elif event.key == pygame.K_q:
                if full_size == False:
                    full_size = True
            elif event.key == pygame.K_r:
                if full_size == True:
                    full_size = False
        #to constantly rotate
        #glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
        #Background Color
        glClearColor(1, 1, 1, 1)
        #draw_axes()

        msg = "Click 'q' to switch to full grid"
        position = -1.5,.8,0
        drawText(position,msg,30)
            
        msg = "Click 'r' to switch to semi grid"
        position = -1.4,.8,0
        drawText(position,msg,30)
            
        column_width = board_size/6.0
        column_center = column_width/2.0
        column_center = .1875
        try:
            if event.type == pygame.MOUSEBUTTONDOWN:
                click_coords = get_mouse_column(board_size)
                move(5 - click_coords[0], 5 - click_coords[1], 1)
                turn_taken = True
                print(click_coords)
                column_indicator(2*-click_coords[1]*column_center+1,2*-click_coords[0]*column_center+1)

                time.sleep(.1)

                msg = "CPU thinking..."
                position = -1.3,.8,0
                drawText(position,msg,30)        
                pygame.display.flip()
                time.sleep(1)
                move(random.randint(0,5), random.randint(0, 5), 2)


        except Exception as e:
            print(e)
            
            
        if full_size == False:
            semi_board(board_size)
        elif full_size == True:
            full_board(board_size)
            
        #render pieces
        for i in range(6):
            for j in range(6):
                for k in range(6):
                    if threedb[i][j][k] == 1:
                        Piece([i,k,j],1)
                    elif threedb[i][j][k] == 2:
                        Piece([i,k,j], 0)


        pygame.display.flip()
    winner = test_game_over()

    #Game finished but you can still look around
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit(); sys.exit()
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                glRotatef(3, 1, 0, 0)
                rotated_xy = rotated_xy + 3
            elif event.key == pygame.K_RIGHT:
                glRotatef(-3, 1, 0, 0)
                rotated_xy = rotated_xy - 3
            elif event.key == pygame.K_UP:
                glRotatef(-3, 0, 0, 1)
                rotate_zy = rotate_zy - 3
            elif event.key == pygame.K_DOWN:
                glRotatef(3, 0, 0, 1)
                rotate_zy = rotate_zy - 3
            #x key undos rotation
            elif event.key == pygame.K_x:
                glRotatef(-rotated_xy, 1, 0, 0)
                glRotatef(-rotate_zy, 0, 0, 1)
                #reset back
                rotated_xy = 0
                rotate_zy = 0
            #z key changes board
            elif event.key == pygame.K_z:
                pygame.display.flip()
                glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
                full_board(board_size)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                pass
            elif event.key == pygame.K_q:
                if full_size == False:
                    full_size = True
            elif event.key == pygame.K_r:
                if full_size == True:
                    full_size = False
        #to constantly rotate
        #glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
        #Background Color
        glClearColor(1, 1, 1, 1)

        
        msg = winner + " won!"
        position = -1.4,.8,0
        drawText(position,msg,30)
            
        column_width = board_size/6.0
        column_center = column_width/2.0
        column_center = .1875

            
            
        if full_size == False:
            semi_board(board_size)
        elif full_size == True:
            full_board(board_size)
            
        #render pieces
        for i in range(6):
            for j in range(6):
                for k in range(6):
                    if threedb[i][j][k] == 1:
                        Piece([i,k,j],1)
                    elif threedb[i][j][k] == 2:
                        Piece([i,k,j], 0)
        pygame.display.flip()

def get_mouse_column(board_size):
    column_width = 2.25/6.0
    column_center = column_width/2.0

    x, y = pygame.mouse.get_pos()
    color_board_a(board_size)
    #print(x, y)
    c = glReadPixels(x, 800 -y, 1.0, 1.0, GL_RGB,GL_UNSIGNED_BYTE, None)
    indi_x = COLORS_BYTES.index(c)# * column_center

    color_board_b(board_size)
    c = glReadPixels(x, 800 -y, 1.0, 1.0, GL_RGB,GL_UNSIGNED_BYTE, None)
    indi_y = COLORS_BYTES.index(c)# * column_center
    return (indi_x, indi_y)
    #print(indi_x, indi_y)

def drawText(position, msg, font_size):
    font = pygame.font.Font (None, font_size)
    textSurface = font.render(msg, True, (0,0,0,255), (255,255,255,255))
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(*position)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)


def color_board_a(size):

            
    #center = (0,0,0)
    #size = 2.25

    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0
    for z in range(num_lines-1):
        #glBegin(GL_LINES)
        glBegin(GL_POLYGON)

        glColor3f(PAINTED_COLORS[z][0], PAINTED_COLORS[z][1], PAINTED_COLORS[z][2])

        #Top lines
        glVertex3f(-size/2.0, size/2.0, size/2.0-(z*column_width))
        glVertex3f(-size/2.0, -size/2.0, size/2.0-(z*column_width))
        
        glVertex3f(-size/2.0, -size/2.0, size/2.0-(z*column_width))
        glVertex3f(-size/2.0, -size/2.0, size/2.0-((z+1)*column_width))

        glVertex3f(-size/2.0, -size/2.0, size/2.0-((z+1)*column_width))
        glVertex3f(-size/2.0, size/2.0, size/2.0-((z+1)*column_width))

        glVertex3f(-size/2.0, size/2.0, size/2.0-((z+1)*column_width))
        glVertex3f(-size/2.0, size/2.0, size/2.0-(z*column_width))

        glEnd()

def color_board_b(size):

            
    #center = (0,0,0)
    #size = 2.25

    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0
    for z in range(num_lines-1):
        #glBegin(GL_LINES)
        glBegin(GL_POLYGON)

        glColor3f(PAINTED_COLORS[z][0], PAINTED_COLORS[z][1], PAINTED_COLORS[z][2])

        #Top lines
        glVertex3f(-size/2.0, size/2.0-(z*column_width), size/2.0)
        glVertex3f(-size/2.0, size/2.0-(z*column_width), -size/2.0)
        
        glVertex3f(-size/2.0, size/2.0-(z*column_width), -size/2.0)
        glVertex3f(-size/2.0, size/2.0-((z+1)*column_width), -size/2.0)
       

        glVertex3f(-size/2.0, size/2.0-((z+1)*column_width), -size/2.0)
        glVertex3f(-size/2.0, size/2.0-((z+1)*column_width), size/2.0)

        glVertex3f(-size/2.0, size/2.0-((z+1)*column_width), size/2.0)
        glVertex3f(-size/2.0, size/2.0-(z*column_width), size/2.0)
        glEnd()


make_board()
main()
