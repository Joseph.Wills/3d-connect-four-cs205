import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
    
def Board():
    #to draw lines
    glBegin(GL_LINES)
    #black lines
    glColor3f(0, 0, 0)
            
    #center = (0,0,0)
    size = 2.25
    
    #Bc there are 6 cols 7 lines are drawn to make them
    num_lines = 7
    column_width = size/6.0
    
    #Render all y-lines
    for z in range(num_lines):
        for x in range(num_lines):
            glVertex3f(-size/2.0+(x*column_width), size/2.0, size/2.0-(z*column_width))
            glVertex3f(-size/2.0+(x*column_width), -size/2.0, size/2.0-(z*column_width))

    #Render all x-lines
    for z in range(num_lines):
        for y in range(num_lines):
            glVertex3f(-size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))
            glVertex3f(size/2.0, size/2.0-(y*column_width), size/2.0-(z*column_width))

    #Render all z-lines
    for y in range(num_lines):
        for x in range(num_lines):
            glVertex3f(-size/2.0+(x*column_width), size/2.0-(y*column_width), size/2.0)
            glVertex3f(-size/2.0+(x*column_width), size/2.0-(y*column_width), -size/2.0)
    
    glEnd()
    
def draw_axes():
    width = 1000
    height = 1000
    
    glLineWidth(2.0);
    glBegin(GL_LINES);
    #Red x-axis
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(-width/2.0, 0.0, 0.0);
    glVertex3f(width/2.0, 0.0, 0.0);
    #Green y-axis
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, height/2.0, 0.0);
    glVertex3f(0.0, -height/2.0, 0.0);
    #Blue z-axis
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, width*2);
    glVertex3f(0.0, 0.0, -width*2);
    glEnd();

    
def main():
    pygame.init()
    pygame.display.set_caption('3D Connect Four')
    window_size = (850,800)
    screen = pygame.display.set_mode(window_size, DOUBLEBUF|OPENGL)
    
    #gluPerspective(field of view, aspect ratio w/h, near & far clipping plane)
    gluPerspective(45, (window_size[0]/window_size[1]), 0.1, 50.0)
    
    glTranslatef(0.0, 0.0, -5)
    glRotatef(0, 0, 0, 0)
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit(); sys.exit()
        
        
        
                    
        glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_COLOR_BUFFER_BIT)
        #Background Color
        glClearColor(1, 1, 1, 1)
        draw_axes()
        Board()
        pygame.display.flip()
        pygame.time.wait(10)
            
main()
